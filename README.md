# Purpose of the Asset : #

To check/uncheck all the checkboxes on a web page using javascript selectors.

### Description of the Asset : ###

* A simple peice of code to check/uncheck all the checkboxes on a webpage using javascript selectors.

### Usage of the Asset : ###

* Customize the query selector as per your need and run the piece of code in browser console. 

### Link to screencast : ###
* https://screencast-o-matic.com/watch/cY61XkKUa3

### Who do I talk to? ###

* Yogesh (yogesh.nama@mtxb2b.com)